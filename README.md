## BleNrf52Uart project
### nRF52 BLE preipheral <-> xamarin central
#### GATTs:
- Over the air firmware update
- UART RX TX
- Battery level
- Custom GATT with notify and write

#### Xamarin
+ MVVM
+ BLE
    - Scan
    - autoconnect
    - disconnect
    - detection connection lost
    - UART RX with CharacteristicReadUart.ValueUpdated event task
    - UART TX
    - get battery value notification
+ messagery central comm
+ Switch behavior to implement a command
    - get and set switch status
+ for the view Strings are in resources file: .resx


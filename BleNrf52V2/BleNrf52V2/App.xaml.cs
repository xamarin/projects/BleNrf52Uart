﻿using System;
using BleNrf52V2.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BleNrf52V2
{
    public partial class App : Application
    {
        public App ()
        {
            InitializeComponent();
            //MainPage = new MainPage();
			MainPage = new MainPageView();
		}

        protected override void OnStart ()
        {
        }

        protected override void OnSleep ()
        {
        }

        protected override void OnResume ()
        {
        }
    }
}


﻿using System;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using BleNrf52V2.ViewModels;
using XamarinEssentials = Xamarin.Essentials;
using System.Windows.Input;
using System.Diagnostics;
using BleNrf52V2.Models;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Plugin.BLE.Abstractions.Exceptions;
using System.Threading;
using System.Text;
using System.Xml.Linq;
using Xamarin.Forms.PlatformConfiguration;
using System.Linq;

/*****************************************************************************
* https://github.com/xabre/xamarin-bluetooth-le
* https://learn.adafruit.com/bluefruit-nrf52-feather-learning-guide/custom-hrm*
*
* nrf52=Peripheral=Server=slave , It advertises its existence
* and a 
* Smartphone=Central=Client=Master : it scans, send a connection request
*
*https://randomnerdtutorials.com/esp32-ble-server-client/
* (https://www.jenx.si/2020/08/13/bluetooth-low-energy-uart-service-with-xamarin-forms)
*****************************************************************************/


namespace BleNrf52V2.DAO
{
   public class BleDAO  //: INotifyPropertyChanged //: ObservableObject
   {
      #region Declarations
      private ObservableCollection<IDevice> ibles;
      private Nrf52UartModel nrf52Device;
      private readonly IAdapter bleAdapter;
      private int myindex = 0;
      #endregion


      #region Constructors
      public BleDAO()
      {
         nrf52Device = new Nrf52UartModel();
         ibles = new ObservableCollection<IDevice>();
         bleAdapter = CrossBluetoothLE.Current.Adapter;
         ReceivedBytes = "Not yet ...";

         //Notify when there is no connection
         bleAdapter.DeviceDisconnected += OnDeviceDisconnected;
         //Notify when connection is lost
         bleAdapter.DeviceConnectionLost += OnDeviceConnectionLost;

         bleAdapter.DeviceDiscovered += (sender, foundBleDevice) =>
         {
            if (foundBleDevice.Device != null && !string.IsNullOrEmpty(foundBleDevice.Device.Name))
            {
               if (foundBleDevice.Device.Name.StartsWith("Arrosage"))
               {
                  ibles.Add(foundBleDevice.Device);
                  //_bluetoothAdapter.StopScanningForDevicesAsync();
               }
               if (ibles.Count > 0)
               {
                  bleAdapter.StopScanningForDevicesAsync();
               }
            }
         };
      }
      #endregion



      #region Methods

      private  void OnDeviceDisconnected(object sender, EventArgs e)
      {
         try
         {
          //  Task tt = BleDisconnectDevices(); //if we stop we reset all
          //  await tt;
            MessagingCenter.Send<BleDAO, string>(this, "BleStatus", "Disconnected...");
            MessagingCenter.Send<BleDAO, string>(this, "BtnScanText", ResourcesStrings.ButtonScan);
            Debug.WriteLine("[BleDAO] --------DISCON-------- OnDeviceDisconnected");
         }
         catch
         {
            Debug.WriteLine("[BleDAO] ERROR --------DISCON-------- OnDeviceDisconnected");
         }
      }

      private  void OnDeviceConnectionLost(object sender, EventArgs e)
      {
         try
         {
            Task tt = BleDisconnectDevices(); //if we stop we reset all
            
            MessagingCenter.Send<BleDAO, string>(this, "BleStatus", "Lost connection...");
            MessagingCenter.Send<BleDAO, string>(this, "BtnScanText", ResourcesStrings.ButtonScan);
            Debug.WriteLine("[BleDAO] ---------LOST------- OnDeviceConnectionLost");
         }
         catch
         {
            Debug.WriteLine("[BleDAO] ERROR -------- LOST -------- OnDeviceConnectionLost");
         }
      }





      public async Task<int> ScanBle() //object toto
      {
         int isok = -1;
         if (!await PermissionsGrantedAsync()) //CHECK FOR PERMS
         {
            Debug.WriteLine("[BleDAO] Permission requiered");
            return isok;
         }
         Debug.WriteLine("[BleDAO] OK PERMS");
         if (await BleDisconnectDevices()) isok = 0;

         DateTime tte;
         DateTime tts = DateTime.Now;
         Debug.WriteLine($"[BleDAO] SCANNING: {tts.Second}");

         try
         {
            ScanFilterOptions scanFilterOptions = new ScanFilterOptions();
            scanFilterOptions.ServiceUuids = new[] { GattNrf52UartModel.GattServiceUart }; // cross platform filter, scanFilterOptions.ManufacturerIds = new[] { 1, 2, 3, etc }; // android only filter
            bleAdapter.ScanTimeout = 25000; //timout of 25sec
            await bleAdapter.StartScanningForDevicesAsync(scanFilterOptions);
            Debug.WriteLine("[BleDAO] AFTER START SCANNING");
            tte = DateTime.Now;
            Debug.WriteLine($"[BleDAO] END SCANNING: {(tte - tts).TotalMilliseconds}");
            Debug.WriteLine($"[BleDAO] >>> Devices count={ibles.Count}");
            foreach (var device in ibles)
            {
               Debug.WriteLine($"[BleDAO] >>> {device.ToString()}: name={device.Name}, Rssi={device.Rssi}");
            }
            isok = ibles.Count;
         }
         catch
         {
            isok = -1;
            Debug.WriteLine($"[BleDAO] ERROR: SCann failed");
         }
         return isok;
      }




      public async void StopScan()
      {
         await bleAdapter.StopScanningForDevicesAsync();
         await BleDisconnectDevices(); //if we stop we reset all
      }



      public async Task<bool> BleConnectDevice()
      {
         bool isok = true;
         // ---------- connection to device ---------------------------------
         try
         {
            var parameters = new ConnectParameters(forceBleTransport: true);
            /*
            var tcs = new CancellationTokenSource();
            await bleAdapter.ConnectToDeviceAsync(SelectedIDevice, parameters, tcs.Token); //SelectedIDevice set by the picker in VM
            //bleAdapter.ConnectToKnownDeviceAsync(new Guid "", parameters, tcs.Token);
            Thread.Sleep(1000); //We cancel the process after 3 seconds.
            tcs.Cancel();
            */
            await bleAdapter.ConnectToDeviceAsync(SelectedIDevice, parameters); //SelectedIDevice set by the picker in VM


            ConnectedDeviceName = SelectedIDevice.Name;
            nrf52Device.ConnectedDevice = SelectedIDevice; //if connection success
            Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC connection ok: {ConnectedDeviceName} doit == {SelectedIDevice.Name}");
         }
         catch
         {
            isok = false;
            ConnectedDeviceName = "Connection error";
            Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC  ERROR connection");
         }

         // ---------- get UART characteristics Read/write ------------------
         try
         {
            if (!String.IsNullOrEmpty(nrf52Device.ConnectedDevice.Name))
            {
               Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC  GUID SERVICE={GattNrf52UartModel.GattServiceUart}");
               nrf52Device.ServiceUart = await nrf52Device.ConnectedDevice.GetServiceAsync(GattNrf52UartModel.GattServiceUart);
               if (nrf52Device.ServiceUart != null)
               {
                  nrf52Device.CharacteristicWriteUart = await nrf52Device.ServiceUart.GetCharacteristicAsync(GattNrf52UartModel.GattCharacteristicWriteUart);
                  nrf52Device.CharacteristicReadUart = await nrf52Device.ServiceUart.GetCharacteristicAsync(GattNrf52UartModel.GattCharacteristicReadUart);

                  if (nrf52Device.CharacteristicReadUart != null)
                  {
                     Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC OK (BleConnectDevice): OK ServiceUart and CharacteristicWriteUart");
                     ConnectedDeviceName = SelectedIDevice.Name;
                     ConnectedDevice = SelectedIDevice;
                  }
               }
               else
               {
                  isok = false;
                  Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC WARNING BleConnectDevice: UART GATT service not found");
                  ConnectedDeviceName = "GATT service not found.";
               }
            }
         }
         catch
         {
            isok = false;
            Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC ERROR BleConnectDevice: Error initializing UART GATT service.");
            ConnectedDeviceName = "Error initializing GATT service.";
         }
         //-------------------------------------------------------------------------------------------------

         // ---------- get Battery characteristics Read ------------------
         try
         {
            if (!String.IsNullOrEmpty(nrf52Device.ConnectedDevice.Name))
            {
               nrf52Device.ServiceBattery = await nrf52Device.ConnectedDevice.GetServiceAsync(GattNrf52UartModel.GattServiceBattery);
               if (nrf52Device.ServiceBattery == null)
               {
                  isok = true;
                  Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC WARNING (BleConnectDevice): BATTERY GATT service not found");
                  ConnectedDeviceName = "GATT service not found.";
               }
               else
               {
                  Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC OK GUID SERVICE={GattNrf52UartModel.GattServiceBattery}");
                  var chars = await nrf52Device.ServiceBattery.GetCharacteristicsAsync(); //var chars = await service.GetCharacteristicsAsync();
                  nrf52Device.CharacteristicReadBattery = chars.FirstOrDefault((c) => c.Id == GattNrf52UartModel.GattCharacteristicsReadBatteryLevel);//GUID_CHAR_BATTERY_LEVEL);//charLevel = chars.FirstOrDefault((c) => c.Id == GUID_CHAR_BATTERY_LEVEL);
                  if (nrf52Device.CharacteristicReadBattery == null)
                     isok = false;
                  Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC OK (ReadBatteryTask): c.Id == GUID_CHAR_BATTERY_LEVEL");
               }
            }
         }
         catch (Exception e)
         {
            isok = false;
            Debug.WriteLine($"[BleDAO] CCCCCCCCCCCCCCCCCCCCC ERROR (BleConnectDevice): Error initializing BATTERY GATT service: {e}");
            ConnectedDeviceName = "Error initializing GATT service.";
         }
         //-------------------------------------------------------------------------------------------------
         return isok;
      }


      public async Task ReadBattery()
      {
         if (nrf52Device.CharacteristicReadBattery == null)
            return;
         try
         {
            await nrf52Device.CharacteristicReadBattery.ReadAsync();
            Debug.WriteLine($"[BleDAO] BBBBBBBBBBBBBBBBBBBBBBBBBBBBB OK (ReadBatteryTask): ReadAsync");
            if ((nrf52Device.CharacteristicReadBattery.Properties & (CharacteristicPropertyType.Notify | CharacteristicPropertyType.Indicate)) > 0)
            {
               showLevel();
            }
         }
         catch (Exception e)
         {
            Debug.WriteLine($"[BleDAO] BBBBBBBBBBBBBBBBBBBBBBBBBBBBB ERROR (ReadBatteryTask): Error : {e}");
         }
      }

      void showLevel()
      {
         if (nrf52Device.CharacteristicReadBattery == null)
            return;
         if (nrf52Device.CharacteristicReadBattery.Value?.Length > 0)
            BatteryLevel = $"btl={string.Format("{0}%", nrf52Device.CharacteristicReadBattery.Value[0])}";
         MessagingCenter.Send<BleDAO, string>(this, "BattLevel", BatteryLevel);
         Debug.WriteLine($"[BleDAO] BBBBBBBBBBBBBBBBBBBBBBBBBBBBB OK (showLevel) Battery Level =[{BatteryLevel}]");
      }

      /*      // We cannot have 2 running task for 2 characteristics......
              private void ReadBatteryTask(object sender, Plugin.BLE.Abstractions.EventArgs.CharacteristicUpdatedEventArgs e)
              {
                  XamarinEssentials.MainThread.BeginInvokeOnMainThread(() =>
                  {
                      showLevel();
                  });
              }
      */


      // ReadCharacteristic not updated in viemodel needs EventHandler or messages!!??
      public async Task UartReceiveBytesTask()
      {
         try
         {
            if (nrf52Device.CharacteristicReadUart != null)
            {
               nrf52Device.CharacteristicReadUart.ValueUpdated += UartValueUpdatedTask;
               await nrf52Device.CharacteristicReadUart.StartUpdatesAsync();
            }
            else
            {
               Debug.WriteLine($"[BleDAO] UUUUUUUUUUUUUUUUUU WARNING UartReceiveBytesTask: nrf52uartDevice.ReadCharacteristic == null");
               ConnectedDeviceName = "GATT service not found.";
            }
         }
         catch (Exception e)
         {
            Debug.WriteLine($"[BleDAO] UUUUUUUUUUUUUUUUUU ERROR UartReceiveBytesTask: Error StartUpdatesAsync. e={e}");
            ConnectedDeviceName = "Error initializing GATT service.";
         }
      }



      private void UartValueUpdatedTask(object sender, Plugin.BLE.Abstractions.EventArgs.CharacteristicUpdatedEventArgs e)
      {
         XamarinEssentials.MainThread.BeginInvokeOnMainThread(() =>
         {
            getUartRX();
         });
      }

      async void getUartRX()
      {
         ReceivedBytes = "Rien";
         if (nrf52Device.CharacteristicReadUart == null)
            return;
         string stmp = nrf52Device.CharacteristicReadUart.StringValue;
         if (stmp?.Length > 0)
         {
            ReceivedBytes = $"Rc:{nrf52Device.CharacteristicReadUart.StringValue}";


            // --------------- read characteristics (battery) -------------------------------------
            //await ReadBattery();
            if (nrf52Device.CharacteristicReadBattery != null)
            {
               int x = 0;
               bool isok = false;
               while (x < 10 && isok == false)
               {
                  isok = true;
                  try
                  {
                     await nrf52Device.CharacteristicReadBattery.ReadAsync();
                     //Debug.WriteLine($"[BleDAO] BBBBBBBBBBBBBBBBBBBBBBBBBBBBB OK (ReadBatteryTask): ReadAsync");
                     if ((nrf52Device.CharacteristicReadBattery.Properties & (CharacteristicPropertyType.Notify | CharacteristicPropertyType.Indicate)) > 0)
                     {
                        showLevel();
                     }
                  }
                  catch (Exception e)
                  {
                     isok = false;
                     Debug.WriteLine($"[BleDAO] BBBBBBBBBBBBBBBBBBBBBBBBBBBBB ERROR (ReadBatteryTask): Error :{x} ");
                     await Task.Delay(50);
                  }
                  x++;
               }
            }
            // --------------- END read characteristics (battery) -------------------------------------

         }
         MessagingCenter.Send<BleDAO, string>(this, "uartR", ReceivedBytes);
         Debug.WriteLine($"[BleDAO] UUUUUUUUUUUUUUUUUU  Recieved myindex={myindex}:[{ReceivedBytes}]");
      }



      public async Task<bool> BleDisconnectDevices()
      {
         bool isok = true;
         foreach (IDevice device in ibles)
         {
            isok = await BleDisconnectDevice(device);
         }
         ibles.Clear();

         return isok;
      }



      /// <summary>
      /// Async Disconnect a specific device
      /// </summary>
      /// <param name="IDevice"></param>
      /// <returns>Boolean</returns>
      public async Task<bool> BleDisconnectDevice(IDevice device)
      {
         bool isok = true;
         try
         {
            if (device.State == DeviceState.Connected)
            {
               await bleAdapter.DisconnectDeviceAsync(device);
               // we reset all properties
               ConnectedDeviceName = "";
               //ReadCharacteristic = null;
               SelectedIDevice = null;
               ConnectedDevice = null;
               ReceivedBytes = "";
               nrf52Device = null; //needed?
               nrf52Device = new Nrf52UartModel();
               isok = true;
               Debug.WriteLine($"[BleDAO] DISCONNECTION OK {device.ToString()}: name={device.Name}, Rssi={device.Rssi}");
            }
         }
         catch (DeviceConnectionException e)
         {
            isok = false;
            Debug.WriteLine($"[BleDAO] DISCONNECTION ERROR {device.ToString()}: name={device.Name}, Rssi={device.Rssi}, {e}");
         }
         return isok;
      }


      public async Task SendLoopAsync()
      {
         try
         {
            if (nrf52Device.CharacteristicWriteUart != null)
            {
               for (int x = 1; x < 100; x++)
               {
                  string str = $"Hi{x.ToString()}";
                  Debug.WriteLine($"[BleDAO] SendCommandButton_Clicked str={str}");
                  var isok = await nrf52Device.CharacteristicWriteUart.WriteAsync(Encoding.ASCII.GetBytes(str));
                  await Task.Delay(2500);
               }
            }
         }
         catch
         {
            Debug.WriteLine($"[BleDAO] ERROR BleConnectDevice: Error sending comand to UART.");
         }
      }


      public async Task<int> SendStringAsync(string str)
      {
         try
         {
            if (nrf52Device.CharacteristicWriteUart != null)
            {

               Debug.WriteLine($"[BleDAO] SendCommandButton_Clicked str={str}");
               var isok = await nrf52Device.CharacteristicWriteUart.WriteAsync(Encoding.ASCII.GetBytes(str));
            }
            else { return 0; }
         }
         catch
         {
            Debug.WriteLine($"[BleDAO] ERROR BleConnectDevice: Error sending comand to UART.");
            return -1;
         }
         return str.Length;
      }


      private async Task<bool> PermissionsGrantedAsync()
      {
         var locationPermissionStatus = await XamarinEssentials.Permissions.CheckStatusAsync<XamarinEssentials.Permissions.LocationAlways>();

         if (locationPermissionStatus != XamarinEssentials.PermissionStatus.Granted)
         {
            var status = await XamarinEssentials.Permissions.RequestAsync<XamarinEssentials.Permissions.LocationAlways>();
            return status == XamarinEssentials.PermissionStatus.Granted;
         }
         return true;
      }
      #endregion







      //************** properties *******************************************
      #region Properties
      public string TextToSend { get; set; }

      //public ICharacteristic ReadCharacteristic { get; set; }


      public IDevice SelectedIDevice { get; set; }

      public string ConnectedDeviceName { get; set; }

      public IDevice ConnectedDevice { get; set; }

      public string ReceivedBytes { get; set; }
      public string BatteryLevel { get; set; }

      public ObservableCollection<IDevice> BleDevices //for the picker
      {
         get
         {
            return ibles;
         }
         set
         {
            ibles = value;
         }
      }
      #endregion


   }
}


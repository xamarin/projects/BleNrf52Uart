﻿using System;
using System.Collections.Generic;
using BleNrf52V2.ViewModels;
using Xamarin.Forms;
using BleNrf52V2.Models;


namespace BleNrf52V2.Views
{
	public partial class MainPageView : ContentPage
	{
		public MainPageView()
		{
			InitializeComponent();
			this.BindingContext = new MainPageViewModel();
        }
	}
}

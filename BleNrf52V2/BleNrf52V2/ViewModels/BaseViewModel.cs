﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;


// https://stackoverflow.com/questions/32667408/how-to-implement-inotifypropertychanged-in-xamarin-forms

namespace BleNrf52V2.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            /*
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
            */
        }
    }
}


﻿using System;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using BleNrf52V2.ViewModels;
using XamarinEssentials = Xamarin.Essentials;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;
using BleNrf52V2.DAO;
using BleNrf52V2.Views;
using System.Collections.ObjectModel;
using System.Text;
using static System.Net.Mime.MediaTypeNames;
using System.Resources;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration.TizenSpecific;

namespace BleNrf52V2.Models
{
   public class MainPageViewModel : BaseViewModel
   {
      #region Declarations
      private BleDAO bleDAO;

      public ICommand BtnVbatClicked { get; set; }
      public ICommand btnScanClicked { get; set; }
      public ICommand btnStopClicked { get; set; }
      public ICommand BtnSendClicked { get; set; }
      public ICommand SwitchToggled { get; set; }

      private bool _isRunning;
      private bool _isBusy;
      private Color _labColor;
      private string _btnScanText;
      private string _bleStaus;
      private string _indicLabelText;
      private string _packedRecieved;
      private string _batteryLevel;
      private bool _indicIsNotRunning;
      private string _textToSend;

      private bool _isSwitchOn;
      #endregion

      #region Constructors
      public MainPageViewModel()
      {
         bleDAO = new BleDAO();

         btnScanClicked = new Command(CmdStartScan, ScanCanValider);
         btnStopClicked = new Command(CmdStopScan, StopCanValider);
         BtnSendClicked = new Command(CmdSendAsync, SendCanValider);
         BtnVbatClicked = new Command(CmdReadVbat, VbatCanValider);

         SwitchToggled = new Command(CmdSwitchToggled, SwitchToggledValider);

         IndicSetRunning = false;
         LabColor = Color.DarkGray;

         PackedRecieved = "";
         BleStatus = ResourcesStrings.StatusNodevice;  //"No Device";
         IndicLabelText = "Scanning...";
         BtnScanText = ResourcesStrings.ButtonScan;
         IndicIsNotRunning = !IndicSetRunning;
         IsBusy = false;
         //https://learn.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/messaging-center
         //https://www.youtube.com/watch?v=JyiL6W-fvEs
         MessagingCenter.Subscribe<BleDAO, string>(this, "uartR", (p, packedRecieved) => { PackedRecieved = packedRecieved; });
         //MessagingCenter.Send<BleDAO, string>(this, "BettLevel", BetteryLevel = stmp);
         MessagingCenter.Subscribe<BleDAO, string>(this, "BattLevel", (p, pc) => { BatteryLevel = pc; });
         MessagingCenter.Subscribe<BleDAO, string>(this, "BleStatus", (p, pc) => { BleStatus = pc; });
         MessagingCenter.Subscribe<BleDAO, string>(this, "BtnScanText", (p, pc) => { BtnScanText = pc; });
         CmdStartScan();
      }
      #endregion


      #region Methods

      /// <summary>
      /// Scan and connect async
      /// </summary>
      /// <returns></returns>
      public async void CmdStartScan()
      {
         if (IsBusy)
            return;
         LabColor = Color.DarkGray;
         int returnval = 0;
         IsBusy = true;
         IndicSetRunning = true;
         IndicIsNotRunning = !IndicSetRunning;
         Debug.WriteLine("[UartGattMV] END btn scan clicked");

         IndicLabelText = "Scanning...";
         BtnScanText = ResourcesStrings.ButtonScan;  //"Scan BLE";
         BleStatus = ResourcesStrings.StatusScanning; //"Scanning..";
         returnval = await bleDAO.ScanBle();
         BleStatus = $"Scan finished return {returnval} devices";
         Debug.WriteLine("[UartGattMV] END Scanning ################### AVANT");
         if (bleDAO.BleDevices.Count > 0)
         {
            Debug.WriteLine($"[UartGattMV] RESULTS ################### btn scan: BleDeviceSelected={BleDeviceSelected}, RetourValidation={BleStatus}");
            BleDeviceSelected = bleDAO.BleDevices[0]; //we use the first device
            await ConnectSelectedBleDevice(); //######## connection
            Debug.WriteLine($"[UartGattMV] RESULTS ################### END CONNECTION");
         }
         else
         {
            BtnScanText = ResourcesStrings.ButtonScan;
            LabColor = Color.Red;
            IndicLabelText = "Scan Without devices";
         }
         IndicSetRunning = false;
         IsBusy = false;
         IndicIsNotRunning = !IndicSetRunning;
      }
      public bool ScanCanValider()
      {
         return !IsBusy;
      }




      public async void CmdStopScan()
      {
         IndicLabelText = "Disconnecting...";
         //IndicSetRunning = true;
         await bleDAO.BleDisconnectDevices();
         bleDAO.StopScan();
         bleDAO.BleDevices.Clear();
         // IndicSetRunning = false;
         BleStatus = ResourcesStrings.StatusNodevice; //"No device...";
      }
      public bool StopCanValider()
      {
         return true;
      }

      public async void CmdReadVbat()
      {
         await bleDAO.ReadBattery();
      }
      public bool VbatCanValider()
      {
         return true;
      }




      /// <summary>
      /// No command for switvh by default
      /// =>we need to implement behavior in order to add command to switch"
      /// in class: SwitchBehavior : Behavior<Switch>
      /// </summary>
      public void CmdSwitchToggled()
      {
         Debug.WriteLine($"[MainViewModel]  ################### CmdSwitchToggled ACTION IsON?=[{IsSwitchOn}]");
      }
      public bool SwitchToggledValider()
      {
         return true;
      }




      public async void CmdSendAsync()
      {
         if (IsBusy)
            return;
         IndicLabelText = "Sending...";
         IsBusy = true;
         int sentsize = await bleDAO.SendStringAsync(TextToSend);
         //await bleDAO.SendLoopAsync();
         //Task tk = bleDAO.UartReceiveBytesTask();
         IsBusy = false;
         IsSwitchOn = false;
      }
      public bool SendCanValider()
      {
         return !IsBusy;// return true;return true;
                        //return !IndicSetRunning;
      }




      public async Task ConnectSelectedBleDevice()
      {
         IsBusy = true;
         IndicSetRunning = true;
         IndicLabelText = "Connecting...";
         BleStatus = $"Connecting {BleDeviceSelected.Name}";
         bool rt = await bleDAO.BleConnectDevice();
         if (!rt)
         {
            LabColor = Color.Red;
            BleStatus = ResourcesStrings.StatusNodevice; //"No device";
            BtnScanText = ResourcesStrings.ButtonScan;
         }

         LabColor = Color.Green;
         if (!(bleDAO.ConnectedDevice is null))
         {
            BleStatus = bleDAO.ConnectedDevice.Name;
            BtnScanText = ResourcesStrings.ButtonRescan;
         }

         await bleDAO.UartReceiveBytesTask();
         //Task t2 = bleDAO.ReadBatteryTask();
         //tt.Wait();

         //Task TT3 = bleDAO.BatteryLevelTask();
         IndicSetRunning = false;
         IsBusy = false;
      }
      #endregion



      // works here, it's ok in the DAO with messages
      //public async Task UartReceiveBytesTask() 

      // #####################@ Properties ###############################

      #region Properties

      /// <summary>
      /// IsToggled Mode=TwoWay is used either to get or set switch on/off
      /// and Toggled is not used anymore
      /// no command for switvh by default=>we need to implement behavior
      /// in order to add command to switch"
      /// </summary>
      public bool IsSwitchOn {
         get { return this._isSwitchOn; }
         set {
            if (this._isSwitchOn != value)
            {
               this._isSwitchOn = value;
               OnPropertyChanged();
            }
         }
      }



      public string IndicLabelText
      {
         get { return this._indicLabelText; }
         set
         {
            if (this._indicLabelText != value)
            {
               this._indicLabelText = value;
               OnPropertyChanged();
            }
         }
      }


      public ObservableCollection<IDevice> ListBles
      {
         get
         {
            return bleDAO.BleDevices;
         }
      }


      public string BtnScanText
      {
         get { return _btnScanText; }
         set
         {
            if (_btnScanText != value)
            {
               _btnScanText = value;
               OnPropertyChanged();
            }
         }
      }


      public IDevice BleDeviceSelected
      {
         get { return bleDAO.SelectedIDevice; }
         set
         {
            if (bleDAO.SelectedIDevice != value)
            {
               bleDAO.SelectedIDevice = value;
               OnPropertyChanged();
            }
         }
      }



      public string BleStatus
      {
         get { return _bleStaus; }
         set
         {
            if (_bleStaus != value)
            {
               _bleStaus = value;
               OnPropertyChanged();
            }
         }
      }

      public String TextToSend
      {
         get
         {
            return _textToSend;
         }
         set
         {
            if (_textToSend != value)
            {
               _textToSend = value;
               OnPropertyChanged();
            }
         }
      }

      public string PackedRecieved
      {
         get
         {
            //return $"\n[{TextToSend}] => [{_packedRecieved}]".Replace("\n", Environment.NewLine);
            return $"[{TextToSend}] => [{_packedRecieved}]";
         }
         set
         {
            if (_packedRecieved != value)
            {
               _packedRecieved = value;
               OnPropertyChanged();
            }
         }
      }


      public string BatteryLevel
      {
         get
         {
            return $"[{_batteryLevel}]";
         }
         set
         {
            if (_batteryLevel != value)
            {
               _batteryLevel = value;
               OnPropertyChanged();
            }
         }
      }

      public bool IsBusy
      {
         get { return _isBusy; }
         set
         {
            _isBusy = value;
            ((Command)btnScanClicked).ChangeCanExecute();
            ((Command)BtnSendClicked).ChangeCanExecute();
            OnPropertyChanged();
         }
      }

      public bool IndicSetRunning
      {
         get { return _isRunning; }
         set
         {
            if (_isRunning != value)
            {
               _isRunning = value;
               ((Command)btnScanClicked).ChangeCanExecute();
               ((Command)BtnSendClicked).ChangeCanExecute();
               OnPropertyChanged();
            }
         }
      }


      public bool IndicIsNotRunning
      {
         get { return _indicIsNotRunning; }
         set
         {
            if (_indicIsNotRunning != value)
            {
               _indicIsNotRunning = value;
               OnPropertyChanged();
            }
         }
      }


      public Color LabColor
      {
         get { return _labColor; }
         set
         {
            if (_labColor != value)
            {
               _labColor = value;
               OnPropertyChanged();
            }
         }
      }

      #endregion


   }
}

﻿using System;
using Plugin.BLE.Abstractions.Contracts;

namespace BleNrf52V2.Models
{
    public class BleDeviceModel
    {
        public BleDeviceModel()
        {
            IsConnected = false;
            ConnectedDeviceName = "";
            this.ConnectedDevice = null;
        }

        public BleDeviceModel(IDevice device)
        {
            IsConnected = false;
            this.ConnectedDevice = device;
            ConnectedDeviceName = "";
        }


        public IDevice ConnectedDevice { get; set; }
        //public IDevice BleDevice {get;set;}
        //public IAdapter BlueToothAdapter {get;set;}
        public bool IsConnected { get; set; }
        //public IService Service { get; set; }
        public string ConnectedDeviceName { get; set; }



        public override string ToString()
        {
            return ConnectedDevice.Name;
        }
    }
}

﻿using System;
using Plugin.BLE.Abstractions.Contracts;

namespace BleNrf52V2.Models
{
    public class Nrf52UartModel: BleDeviceModel
    {

        // for the nRF52 Nordic
        public Nrf52UartModel()
        {
            ServiceUart = null;
            ServiceBattery = null;
        }

        public IService ServiceUart { get; set; }
        public ICharacteristic CharacteristicWriteUart { get; set; }
        public ICharacteristic CharacteristicReadUart { get; set; }


        public IService ServiceBattery { get; set; }
        public ICharacteristic CharacteristicReadBattery { get; set; }
    }
}


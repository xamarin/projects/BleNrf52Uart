﻿using System;
namespace BleNrf52V2.Models
{
   public static class GattNrf52UartModel
   {
      // for the nRF52 Nordic
      // https://stackoverflow.com/questions/53681336/bluetooth-le-service-uuid-and-characteristic-uuid-format
      // some generators: https://www.uuidgenerator.net/

      // --------------------------- UART GATT -----------------------------------
      private static Guid gattServiceUart = Guid.Parse("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
      private static Guid gattCharacteristicReadUart = Guid.Parse("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"); //6e400003-b5a3-f393-e0a9-e50e24dcca9e
      private static Guid gattCharacteristicWriteUart = Guid.Parse("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
      //public static Guid SpecialNotificationDescriptorId = Guid.Parse ("00002902-0000-1000-8000-00805f9b34fb");

      public static Guid GattServiceUart { get { return gattServiceUart; } set { gattServiceUart = value; } }
      public static Guid GattCharacteristicReadUart { get { return gattCharacteristicReadUart; } }
      public static Guid GattCharacteristicWriteUart { get { return gattCharacteristicWriteUart; } }

      // --------------------------- for custom GATT -----------------------------------
      //button led Service Definitions

      /* my io Service: 00001023-1212-EFDE-1523-785FEABCD123
       * char Button : 00001024-1212-EFDE-1523-785FEABCD123
       * char LED    : 00001025-1212-EFDE-1523-785FEABCD123
       */
      private static Guid gattServiceIO = Guid.Parse("00001023-1212-EFDE-1523-785FEABCD123");
      private static Guid gattCharacteristicsNotifyButton = Guid.Parse("00001024-1212-EFDE-1523-785FEABCD123");
      private static Guid gattCharacteristicsWriteLed = Guid.Parse("00001025-1212-EFDE-1523-785FEABCD123");

      public static Guid GattServiceIO { get { return gattServiceIO; }}
      public static Guid GattCharacteristicsNotifyButton { get { return gattCharacteristicsNotifyButton; } }
      public static Guid GattCharacteristicsWriteLed { get { return gattCharacteristicsWriteLed; } }
      

      //------------ Battery service & read characteristics --------------------------------------------
      private static Guid gattServiceBattery = Guid.Parse("0000180F-0000-1000-8000-00805F9B34FB");
      private static Guid gattCharacteristicsReadBatteryLevel = Guid.Parse("00002A19-0000-1000-8000-00805F9B34FB");
      public static Guid GattServiceBattery { get { return gattServiceBattery; } set { gattServiceBattery = value; } }
      public static Guid GattCharacteristicsReadBatteryLevel { get { return gattCharacteristicsReadBatteryLevel; } }

   }
}


﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BleNrf52V2.DAO;
using BleNrf52V2.Models;
using Plugin.BLE.Abstractions.Contracts;

namespace consoleproject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Task tt= Method1();
            Method2();
            BleDAO m1DAO = new BleDAO ();

			System.Console.WriteLine ("------------------List all values -------------------\n");
            ObservableCollection<IDevice> ms = m1DAO.BleDevices;
			foreach (var m in ms) {
				System.Console.WriteLine ($"elid: {m.Id}, elNom: {m.Name}");
			}

		}


        //https://www.c-sharpcorner.com/article/async-and-await-in-c-sharp/
        public static async Task Method1()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine($" Method 1:{i}");
                    // Do something
                    Task.Delay(100).Wait();
                }
            });
        }


        public static void Method2()
        {
            for (int i = 0; i < 25; i++)
            {
                Console.WriteLine($" Method 2:{i}");
                // Do something
                Task.Delay(100).Wait();
            }
        }

    }
}


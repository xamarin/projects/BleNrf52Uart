#!/bin/bash

newname=$1
oldname=$2 #"BleNrf52V2"

echo "migrate project from ${oldname} to $newname"

#TODO check parameters & usage

echo "####################### remove cache & Debug #########################"
find . -type d -name '*.cache' -exec rm -fr {} \;
find . -iname '*.cache' -exec rm -f {} \;
find . -type d -name '*Debug' -exec rm -fr {} \;
find . -type d -name '*Release' -exec rm -fr {} \;

echo "####################### mv folders >3 pass for the depth #########################"
# mv folders >3 pass for the depth
for c in {1..5}
do
	for i in $(find . -type d -name '*'${oldname}'*')
	do
		n=$(echo $i|sed 's@\b'${oldname}'\b@'${newname}'@g')
		echo -en $i
		echo " ==> $n"
		mv -f $i $n
	done
done


echo "####################### rename files #########################"
for c in {1..2}
do
	for i in $(find . -type f -name '*'${oldname}'.*')
	do
		echo -en "$i"
		n=$(echo $i|sed 's/\b'${oldname}'\b/'${newname}'/g')
		echo " ==> $n"
		mv -f $i $n
	done
done

echo "####################### replace in file #########################"
for i in $(find . -type f)
do
	echo $i
	sed -i 's/'${oldname}'/'${newname}'/g' $i
done

exit 0
